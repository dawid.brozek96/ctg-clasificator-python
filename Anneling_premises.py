import numpy as np
import Conclusion as Conc
import classify
import scipy.io
import os


def multiple_appends(listname, *element):
    listname.extend(element)


def Anneling_mod(data_name=None, v=None, s=None, mergedU=None, mergedT=None, nrule=None, t=None, a=None, first_mean_qi=None, L=None):

    Global_Table_Best_parameters = []

    for Era_length in range (0 , L):

        while True:

            neighborhood_matrix_s = s + np.random.randn(12, nrule)  # TODO nrule
            neighborhood_matrix_v = v + np.random.randn(12, nrule)

            neighborhood_matrix_v[neighborhood_matrix_v < 0] = 0
            neighborhood_matrix_v[neighborhood_matrix_v > 1] = 1
            neighborhood_matrix_s[neighborhood_matrix_s < 0] = np.finfo(float).eps

            table_alg_qi = np.empty((0,1), float)
            for k1 in range(1, 6):
                if k1 == 1:
                    data_ku = mergedU[:275, :]
                    data_kt = mergedT[:276, :]
                elif k1==2:
                    data_ku = mergedU[275:550, :]
                    data_kt = mergedT[276:552, :]
                elif k1==3:
                    data_ku = mergedU[550:825, :]
                    data_kt = mergedT[552:828, :]
                elif k1==4:
                    data_ku = mergedU[825:1100, :]
                    data_kt = mergedT[828:1104, :]
                elif k1==5:
                    data_ku = mergedU[1100:1375, :]
                    data_kt = mergedT[1104:1380, :]

                p_alg = Conc.RMNK(data_ku, neighborhood_matrix_v, neighborhood_matrix_s, nrule)

                qi_alg, acc, cz = classify.QI_Prawdziwe(data_kt, p_alg, neighborhood_matrix_v,
                                                        neighborhood_matrix_s, nrule)
                qi1_alg = np.array([[qi_alg]])

                table_alg_qi = np.append(table_alg_qi, qi1_alg, axis=0)

            alg_mean_qi = np.mean(table_alg_qi)
            if alg_mean_qi > first_mean_qi:

                v = neighborhood_matrix_v
                s = neighborhood_matrix_s

                table_best_parameters = [alg_mean_qi]
                multiple_appends(table_best_parameters, neighborhood_matrix_v, neighborhood_matrix_s)
                table_best_parameters = np.transpose(table_best_parameters)

                multiple_appends(Global_Table_Best_parameters, table_best_parameters)

            else:
                y = np.random.rand(1,1)
                if y < np.exp((-(alg_mean_qi - first_mean_qi)) / t):
                    v= neighborhood_matrix_v
                    s = neighborhood_matrix_s
            if alg_mean_qi > first_mean_qi: break
        first_mean_qi = alg_mean_qi
        t = a * t

    Global_Table_Best_parameters.sort(key=lambda x: x[0])

    global_table_best_parameter = Global_Table_Best_parameters[len(Global_Table_Best_parameters)-1]

    v_best = global_table_best_parameter[1]
    s_best = global_table_best_parameter[2]


    # @@@@@@
    # return vb, sb @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ TODO 2 files/ functions?
    # @@@@@@

    v_db_qi = np.array([[0], [np.array([0])]])
    sum_v_db_qi=0
    sum_v_db_acc=0
    sum_v_db_ten=0
    for index in range (1, 51): # Bcs there is 50x trainig / learining data set
        data_name_number = "{data_name}{index}.mat".format(data_name=data_name, index=index)

        path_ref = os.path.join('reference_data', data_name_number)
        mat_ref = scipy.io.loadmat(path_ref)

        data_reference_u = 'daneU_' + data_name
        data_reference_t = 'daneT_' + data_name

        reference_data_learning = mat_ref.get(data_reference_u)

        reference_data_test = mat_ref.get(data_reference_t)

        conclusion_v_db = Conc.RMNK(reference_data_learning, v_best, s_best, nrule)
        out_v_db_qi, out_v_db_acc, out_v_db_ten = classify.QI_Prawdziwe(reference_data_test, conclusion_v_db, v_best, s_best, nrule)

        table_out_v_db = np.array([[out_v_db_qi], [conclusion_v_db]])

        sum_v_db_qi = sum_v_db_qi + out_v_db_qi
        sum_v_db_acc = sum_v_db_acc + out_v_db_acc
        sum_v_db_ten = sum_v_db_ten + out_v_db_ten

        v_db_qi = np.append(v_db_qi, table_out_v_db, axis=0)

    v_db_qi = np.reshape(v_db_qi, (51,2)) #TODO do 51,2

    v_db_qi = v_db_qi.tolist()
    v_db_qi.sort(key=lambda x: x[0])
    v_db_qi_best = v_db_qi[len(v_db_qi)-1]
    conclusion_v_db = v_db_qi_best[1]

    sum_p_db_qi = 0
    sum_p_db_acc = 0
    sum_p_db_ten = 0

    for index in range(1, 51):  # bcs we have 51 data sets (training/learning)
        data_name_number = "{data_name}{index}.mat".format(data_name=data_name, index=index)

        path_ref = os.path.join('reference_data', data_name_number)
        mat_ref = scipy.io.loadmat(path_ref)

        data_reference_t = 'daneT_' + data_name

        reference_data_test = mat_ref.get(data_reference_t)
        out_p_db_qi, out_p_db_acc, out_p_db_ten = classify.QI_Prawdziwe(reference_data_test, conclusion_v_db, v_best, s_best, nrule)

        sum_p_db_qi = sum_p_db_qi + out_p_db_qi
        sum_p_db_acc = sum_p_db_acc + out_p_db_acc
        sum_p_db_ten = sum_p_db_ten + out_p_db_ten

    v_db_qi = sum_v_db_qi / 50
    v_db_acc = sum_v_db_acc / 50
    v_db_ten = sum_v_db_ten / 50

    p_db_qi = sum_p_db_qi / 50
    p_db_acc = sum_p_db_acc / 50
    p_db_ten = sum_p_db_ten / 50

    return v_db_qi, v_db_acc, v_db_ten, p_db_qi, p_db_acc, p_db_ten
