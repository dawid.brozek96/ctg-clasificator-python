import numpy
import Conclusion as Conc
import classify
import Import_premises
import Anneling_premises
import time

start_time = time.time()

while True:
   try:
       choose_reference = int(input("Choose a reference grade: 1 = Percentyl, 2 = Ph, 3 = Apgar: "))
   except ValueError:
       print ('Thats not a number!')
   else:
       if (0 < choose_reference < 4):
            choices = {1: 'Percentyl', 2: 'Ph', 3: 'Apgar'}
            result = choices.get(choose_reference, 'default')
            break
       else:
           print ('Out of range. Try again')

data_name = result

while True:
   try:
       nrule = int(input("Enter an even number of rules between 4 and 20: "))
   except ValueError:
       print ('Thats not a number!')
   else:
       if (3 < nrule < 21):
           if (nrule % 2 == 0):
               break
           else:
               print('The number must be even. Try again')
       else:
           print('Out of range. Try again')

a=0.99  # range (0.8, 0.99)
T=10  # range (1, 10E16)
Era= 3  # range (1, 50), more = longer

dataC, dataS, mergedU, mergedT = Import_premises.Initial_premises(data_name, nrule)

v1 = numpy.transpose(dataC)
s1 = numpy.transpose(dataS)

table_first_QI = numpy.empty((0,1), float)
for k1 in range(1, 6):
    if k1 == 1:
        dataKu = mergedU[ :275, :]
        dataKt = mergedT[ :276, :]
    elif k1==2:
        dataKu = mergedU[275:550, :]
        dataKt = mergedT[276:552, :]
    elif k1==3:
        dataKu = mergedU[550:825, :]
        dataKt = mergedT[552:828, :]
    elif k1==4:
        dataKu = mergedU[825:1100, :]
        dataKt = mergedT[828:1104, :]
    elif k1==5:
        dataKu = mergedU[1100:1375, :]
        dataKt = mergedT[1104:1380, :]

    p1 = Conc.RMNK(dataKu, v1, s1, nrule)

    Qi1, acc, cz = classify.QI_Prawdziwe(dataKt, p1, v1, s1, nrule)

    Qi1 = numpy.array([[Qi1]])
    table_first_QI = numpy.append(table_first_QI, Qi1, axis=0)

First_mean_Qi = numpy.mean(table_first_QI)

print('\n', 'Condition in algorithm = ', First_mean_Qi, '\n')
v = v1
s = s1
dataC = numpy.transpose(dataC)
dataS = numpy.transpose(dataS)
vDB_Qi, vDB_Acc, vDB_Ten, pDB_Qi, pDB_Acc, pDB_Ten = Anneling_premises.Anneling_mod(data_name, dataC, dataS,
                                                                                    mergedU, mergedT, nrule,
                                                                                    T, a, First_mean_Qi, Era)

print ('QI vDB = ', vDB_Qi )
print ('Accurate vDB = ', vDB_Acc)
print ('Tenderness vDB = ', vDB_Ten, '\n')

print ('QI pDB = ', pDB_Qi )
print ('Accurate pDB = ', pDB_Acc )
print ('Tenderness pDB = ', pDB_Ten, '\n')

print("--- %s seconds ---" % (time.time() - start_time))