import numpy as np


def RMNK(data=None, c=None, s=None, nrule=None):

    [n1, m1] = data.shape

    g = 10E6 * np.eye(m1 * nrule)
    p_peak = np.zeros((m1 * nrule, 1))

    for n in range(0, n1):

        diff1 = np.dot(np.transpose(data[n:n+1, 0:m1-1]), np.ones((1, nrule))) - c

        f = np.transpose(np.exp(-0.5 * np.sum(np.power((diff1/s), 2), axis=0)))
        f = np.reshape(f, (1, nrule))

        if (np.sum(f) < np.finfo(float).eps):

            f = np.ones((1, nrule)) * np.finfo(float).eps

        f_peak = f / np.sum(f)

        d1 = data[n:n+1, 0:m1-1] #TODO change it
        d1 = np.c_[d1, 1]
        d1 = np.reshape(d1, (1, 13))
        d1 = np.transpose(d1)

        f_peak = np.reshape(f_peak, (1, nrule))

        d1 = np.dot(d1, f_peak)

        d = np.reshape(d1, (nrule*13, 1))
        d_trans = np.transpose(d)

        g = g - ((g * d * d_trans *g)/(d_trans * g * d + 1))

        p_help = np.dot(d_trans, p_peak) #TODO change it!
        p_helpp= data[n-1, m1-1] - p_help
        p_help2 = np.dot(g, d)
        p_help3 = np.dot(p_help2, p_helpp)
        p_peak = (p_peak + p_help3)

    return p_peak

