import scipy.io
import numpy as np
import os


def Initial_premises(data_name, nrule):

    nrule = int(nrule/2)

    premises_number_name = "p_start_m11_c{nrule}_norm{data_name}.mat".format(nrule=nrule, data_name=data_name)
    path_premises = os.path.join('start premises', premises_number_name)

    mat_premises = scipy.io.loadmat(path_premises)

    s1 = mat_premises.get('S1')
    s2 = mat_premises.get('S2')
    v1 = mat_premises.get('V1')
    v2 = mat_premises.get('V2')

    data_s = np.append(s1, s2)
    data_s = np.reshape(data_s, (2 * nrule, 12))

    data_c = np.append(v1, v2)
    data_c = np.reshape(data_c, (2 * nrule, 12))

    merged_u_name = "mergedU_{data_name}.mat".format(data_name=data_name)  # TODO mrule  nrule/2
    path_merged_u = os.path.join('merged_data', merged_u_name)
    mat_merged_u = scipy.io.loadmat(path_merged_u)
    merged_u = mat_merged_u.get('mergedU')

    merged_t_name = "mergedT_{data_name}.mat".format(data_name=data_name)  # TODO mrule  nrule/2
    path_merged_t = os.path.join('merged_data', merged_t_name)
    mat_merged_t = scipy.io.loadmat(path_merged_t)
    merged_t = mat_merged_t.get('mergedT')

    return data_c, data_s, merged_u, merged_t



