import numpy as np


def QI_Prawdziwe(data = None, P_daszek=None, c=None, s=None, nrule=None):

    [n1, m1] = data.shape
    table_calssify=np.zeros((n1, 1))

    for n in range(0, n1):

        diff1 = np.dot(np.transpose(data[n:n + 1, 0:m1 - 1]), np.ones((1, nrule))) - c
        f = np.exp(-0.5 * np.sum(np.power((diff1 / s), 2), axis=0))

        if (np.sum(f) < np.finfo(float).eps):
            f = np.ones((1, nrule)) * np.finfo(float).eps

        f_daszek = f/np.sum(f)
        d1 = data[n:n + 1, 0:m1 - 1] #TODO change iit
        d1 = np.c_[d1, 1]
        d1 = np.reshape(d1, (1, 13))
        d1 = np.transpose(d1)
        f_daszek = np.reshape(f_daszek, (1, nrule))

        d1 = np.dot(d1, f_daszek)

        d = np.reshape(d1, (13*nrule, 1))
        d_trans = np.transpose(d)
        y_spr2=np.dot(d_trans,  P_daszek)
        table_calssify[n, :] = [y_spr2]

    sum_pp_vec = 0
    sum_pn_vec = 0
    sum_fp_vec = 0
    sum_fn_vec = 0

    for n in range(0, n1):  # TODO change it
        etykiety_rmnk = 2*(table_calssify[n] > 0)-1
        etykiety_dane = data[n, m1-1]

        if etykiety_dane >= 0:
            etykiety_dane = 1
        else:
            etykiety_dane= -1

        if etykiety_rmnk >= 0:
            etykiety_rmnk = 1
        else:
            etykiety_rmnk= -1

        if ( etykiety_rmnk== -1) and ( etykiety_dane == -1):
            sum_pp_vec = sum_pp_vec + 1

        if (etykiety_rmnk == 1) and (etykiety_dane == 1):
            sum_pn_vec = sum_pn_vec + 1

        if (etykiety_rmnk == 1) and (etykiety_dane == -1):

            sum_fn_vec = sum_fn_vec + 1

        if (etykiety_rmnk == -1) and (etykiety_dane == 1):
            sum_fp_vec = sum_fp_vec + 1

    specificity = (sum_pn_vec / (sum_pn_vec + sum_fp_vec)) * 100
    tenderess = (sum_pp_vec / (sum_pp_vec + sum_fn_vec)) * 100
    out_true = np.sqrt(specificity * tenderess)
    accurate = ((sum_pn_vec + sum_pp_vec)/(sum_pn_vec + sum_pp_vec + sum_fn_vec + sum_fp_vec))*100

    return out_true, accurate, tenderess
